/** Generic number array type */
export type NumberArray = {
  [index: number]: number;
};

export type NumberBool = 0 | 1;

// Maximum number of channels in ICC Profiles
export var cmsMAXCHANNELS: number; // L654

export var cmsSigRedColorantTag: number; // 'rXYZ' (L382)
export var cmsSigGreenColorantTag: number; // 'gXYZ'
export var cmsSigBlueColorantTag: number; // 'bXYZ'

// ICC Color spaces
export var cmsSigXYZData: number; // 'XYZ ' (L433)
export var cmsSigLabData: number; // 'Lab '
export var cmsSigLuvData: number; // 'Luv '
export var cmsSigYCbCrData: number; // 'YCbr'
export var cmsSigYxyData: number; // 'Yxy '
export var cmsSigRgbData: number; // 'RGB '
export var cmsSigGrayData: number; // 'GRAY'
export var cmsSigHsvData: number; // 'HSV '
export var cmsSigHlsData: number; // 'HLS '
export var cmsSigCmykData: number; // 'CMYK'
export var cmsSigCmyData: number; // 'CMY '

// Pixel types
export var PT_ANY: number; // Don't check colorspace // (L701)
// 1 & 2 are reserved
export var PT_GRAY: number;
export var PT_RGB: number;
export var PT_CMY: number;
export var PT_CMYK: number;
export var PT_YCbCr: number;
export var PT_YUV: number; // Lu'v'
export var PT_XYZ: number;
export var PT_Lab: number;
export var PT_YUVK: number; // Lu'v'K
export var PT_HSV: number;
export var PT_HLS: number;
export var PT_Yxy: number;

export var PT_MCH1: number;
export var PT_MCH2: number;
export var PT_MCH3: number;
export var PT_MCH4: number;
export var PT_MCH5: number;
export var PT_MCH6: number;
export var PT_MCH7: number;
export var PT_MCH8: number;
export var PT_MCH9: number;
export var PT_MCH10: number;
export var PT_MCH11: number;
export var PT_MCH12: number;
export var PT_MCH13: number;
export var PT_MCH14: number;
export var PT_MCH15: number;

export var PT_LabV2: number; // Identical to PT_Lab, but using the V2 old encoding

export var TYPE_GRAY_8: number; // (L739)
export var TYPE_GRAY_8_REV: number;
export var TYPE_GRAY_16: number;
export var TYPE_GRAY_16_REV: number;
export var TYPE_GRAY_16_SE: number;
export var TYPE_GRAYA_8: number;
export var TYPE_GRAYA_16: number;
export var TYPE_GRAYA_16_SE: number;
export var TYPE_GRAYA_8_PLANAR: number;
export var TYPE_GRAYA_16_PLANAR: number;

export var TYPE_RGB_8: number;
export var TYPE_RGB_8_PLANAR: number;
export var TYPE_BGR_8: number;
export var TYPE_BGR_8_PLANAR: number;
export var TYPE_RGB_16: number;
export var TYPE_RGB_16_PLANAR: number;
export var TYPE_RGB_16_SE: number;
export var TYPE_BGR_16: number;
export var TYPE_BGR_16_PLANAR: number;
export var TYPE_BGR_16_SE: number;

export var TYPE_RGBA_8: number;
export var TYPE_RGBA_8_PLANAR: number;
export var TYPE_RGBA_16: number;
export var TYPE_RGBA_16_PLANAR: number;
export var TYPE_RGBA_16_SE: number;

export var TYPE_ARGB_8: number;
export var TYPE_ARGB_8_PLANAR: number;
export var TYPE_ARGB_16: number;

export var TYPE_ABGR_8: number;
export var TYPE_ABGR_8_PLANAR: number;
export var TYPE_ABGR_16: number;
export var TYPE_ABGR_16_PLANAR: number;
export var TYPE_ABGR_16_SE: number;

export var TYPE_BGRA_8: number;
export var TYPE_BGRA_8_PLANAR: number;
export var TYPE_BGRA_16: number;
export var TYPE_BGRA_16_SE: number;

export var TYPE_CMY_8: number;
export var TYPE_CMY_8_PLANAR: number;
export var TYPE_CMY_16: number;
export var TYPE_CMY_16_PLANAR: number;
export var TYPE_CMY_16_SE: number;

export var TYPE_CMYK_8: number;
export var TYPE_CMYKA_8: number;
export var TYPE_CMYK_8_REV: number;
export var TYPE_YUVK_8: number;
export var TYPE_CMYK_8_PLANAR: number;
export var TYPE_CMYK_16: number;
export var TYPE_CMYK_16_REV: number;
export var TYPE_YUVK_16: number;
export var TYPE_CMYK_16_PLANAR: number;
export var TYPE_CMYK_16_SE: number;

export var TYPE_KYMC_8: number;
export var TYPE_KYMC_16: number;
export var TYPE_KYMC_16_SE: number;

export var TYPE_KCMY_8: number;
export var TYPE_KCMY_8_REV: number;
export var TYPE_KCMY_16: number;
export var TYPE_KCMY_16_REV: number;
export var TYPE_KCMY_16_SE: number;

export var TYPE_CMYK5_8: number;
export var TYPE_CMYK5_16: number;
export var TYPE_CMYK5_16_SE: number;
export var TYPE_KYMC5_8: number;
export var TYPE_KYMC5_16: number;
export var TYPE_KYMC5_16_SE: number;
export var TYPE_CMYK6_8: number;
export var TYPE_CMYK6_8_PLANAR: number;
export var TYPE_CMYK6_16: number;
export var TYPE_CMYK6_16_PLANAR: number;
export var TYPE_CMYK6_16_SE: number;
export var TYPE_CMYK7_8: number;
export var TYPE_CMYK7_16: number;
export var TYPE_CMYK7_16_SE: number;
export var TYPE_KYMC7_8: number;
export var TYPE_KYMC7_16: number;
export var TYPE_KYMC7_16_SE: number;
export var TYPE_CMYK8_8: number;
export var TYPE_CMYK8_16: number;
export var TYPE_CMYK8_16_SE: number;
export var TYPE_KYMC8_8: number;
export var TYPE_KYMC8_16: number;
export var TYPE_KYMC8_16_SE: number;
export var TYPE_CMYK9_8: number;
export var TYPE_CMYK9_16: number;
export var TYPE_CMYK9_16_SE: number;
export var TYPE_KYMC9_8: number;
export var TYPE_KYMC9_16: number;
export var TYPE_KYMC9_16_SE: number;
export var TYPE_CMYK10_8: number;
export var TYPE_CMYK10_16: number;
export var TYPE_CMYK10_16_SE: number;
export var TYPE_KYMC10_8: number;
export var TYPE_KYMC10_16: number;
export var TYPE_KYMC10_16_SE: number;
export var TYPE_CMYK11_8: number;
export var TYPE_CMYK11_16: number;
export var TYPE_CMYK11_16_SE: number;
export var TYPE_KYMC11_8: number;
export var TYPE_KYMC11_16: number;
export var TYPE_KYMC11_16_SE: number;
export var TYPE_CMYK12_8: number;
export var TYPE_CMYK12_16: number;
export var TYPE_CMYK12_16_SE: number;
export var TYPE_KYMC12_8: number;
export var TYPE_KYMC12_16: number;
export var TYPE_KYMC12_16_SE: number;

// Colorimetric
export var TYPE_XYZ_16: number;
export var TYPE_Lab_8: number;
export var TYPE_LabV2_8: number;

export var TYPE_ALab_8: number;
export var TYPE_ALabV2_8: number;
export var TYPE_Lab_16: number;
export var TYPE_LabV2_16: number;
export var TYPE_Yxy_16: number;

// Floating point formatters.
// NOTE THAT 'BYTES' FIELD IS SET TO ZERO ON DLB because 8 bytes overflows the bitfield
export var TYPE_XYZ_DBL: number; // (L916)
export var TYPE_Lab_DBL: number;
export var TYPE_GRAY_DBL: number;
export var TYPE_RGB_DBL: number;
export var TYPE_BGR_DBL: number;
export var TYPE_CMYK_DBL: number;

// Localized info, enum cmsInfoType
export var cmsInfoDescription: number; // (L1503)
export var cmsInfoManufacturer: number;
export var cmsInfoModel: number;
export var cmsInfoCopyright: number;

// ICC Intents
export var INTENT_PERCEPTUAL: number; // (L1617)
export var INTENT_RELATIVE_COLORIMETRIC: number;
export var INTENT_SATURATION: number;
export var INTENT_ABSOLUTE_COLORIMETRIC: number;

// Flags
export var cmsFLAGS_NOCACHE: number; // Inhibit 1-pixel cache (L1636)
export var cmsFLAGS_NOOPTIMIZE: number; // Inhibit optimizations
export var cmsFLAGS_NULLTRANSFORM: number; // Don't transform anyway

// Proofing flags
export var cmsFLAGS_GAMUTCHECK: number; // Out of Gamut alarm
export var cmsFLAGS_SOFTPROOFING: number; // Do softproofing

// Misc
export var cmsFLAGS_BLACKPOINTCOMPENSATION: number;
export var cmsFLAGS_NOWHITEONWHITEFIXUP: number; // Don't fix scum dot
export var cmsFLAGS_HIGHRESPRECALC: number; // Use more memory to give better accurancy
export var cmsFLAGS_LOWRESPRECALC: number; // Use less memory to minimize resources

export function cmsOpenProfileFromMem(arr: Uint8Array, size: number): number;
export function cmsCloseProfile(hProfile: number): number;
export function cmsCreate_sRGBProfile(): number;
export function cmsCreateXYZProfile(): number;
export function cmsCreateLab4Profile(wpArr: [number, number, number]): number;

/** usage: hInput, cmsInfoDescription, "en", "US" */
export function cmsGetProfileInfoASCII(
  hProfile: number,
  info: number,
  languageCode: string,
  countryCode: string
): string;

export function cmsGetColorSpace(hProfile: number): number;

export function cmsFormatterForColorspaceOfProfile(
  hProfile: number,
  nBytes: number,
  isFloat: NumberBool
): number;

export function cmsCreateTransform(
  hInput: number,
  inputFormat: number,
  hOutput: number,
  outputFormat: number,
  intent: number,
  flags: number
): number;

export function cmsCreateProofingTransform(
  hInput: number,
  inputFormat: number,
  hOutput: number,
  outputFormat: number,
  proofing: number,
  intent: number,
  proofingIntent: number,
  flags: number
): number;

export function cmsDeleteTransform(transform: number): void;
export function cmsGetTransformInputFormat(transform: number): number;
export function cmsGetTransformOutputFormat(transform: number): number;

/**
 * The size parameter is the amount of pixels in
 * the data array (array length / channels).
 *
 * Returns a Float64Array if the transform's output format
 * is in floats and a Uint16Array otherwise.
 */
export function cmsDoTransform(
  transform: number,
  inputArr: NumberArray,
  size: number // array length / channels
): Float64Array | Uint16Array;

export function cmsReadTag(hProfile: number, sig: number): number;
export function cmsReadTag_XYZ(hProfile: number, sig: number): Float64Array;
export function cmsXYZ2xyY(xyz: [number, number, number]): Float64Array;
