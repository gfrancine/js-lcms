# js-lcms

js-lcms is a fork of [lcms.js](https://github.com/yoya/lcms.js/), a low-level binding to [LittleCMS](https://github.com/mm2/Little-CMS) based on emscripten.

## Example Usage

The example below shows how to convert an RGB array from an sRGB profile to another sRGB profile:

```js
import {
  INTENT_RELATIVE_COLORIMETRIC,
  cmsCreate_sRGBProfile,
  cmsFormatterForColorspaceOfProfile,
  cmsCreateTransform,
  cmsDoTransform,
  cmsDeleteTransform,
  cmsCloseProfile,
} from "js-lcms";

const inputProfile = cmsCreate_sRGBProfile();
const outputProfile = cmsCreate_sRGBProfile();
const isFloat = 0; // false

const inputFormat = cmsFormatterForColorspaceOfProfile(
  inputProfile,
  isFloat ? 0 : 2,
  isFloat
);

const outputFormat = cmsFormatterForColorspaceOfProfile(
  outputProfile,
  isFloat ? 0 : 2,
  isFloat
);

const transform = cmsCreateTransform(
  profile,
  inputFormat,
  profile,
  outputFormat,
  INTENT_RELATIVE_COLORIMETRIC,
  0
);

const image = Uint8Array.from([0, 127, 255]);
console.log(cmsDoTransform(transform, image, image.length / 3));

cmsDeleteTransform(transform);
cmsCloseProfile(inputProfile);
cmsCloseProfile(outputProfile);
```

## Web Demo

A print-to-console web demo can be found in the `./demo` directory. Run `npm run demo` to build it and start a local server.

## Other Examples and Resources

LCMS.js color conversion demo by @yoya:

- http://app.awm.jp/image.js/lcms.html
- https://github.com/yoya/image.js/blob/master/lcms.js

LittleCMS 2.12 documentation:

- https://www.littlecms.com/LittleCMS2.12%20tutorial.pdf
- https://www.littlecms.com/LittleCMS2.12%20API.pdf

## Developing

Make sure you have git and [emscripten](https://emscripten.org/docs/getting_started/downloads.html) installed on your system.

js-lcms works by building LittleCMS to JavaScript with emscripten and directly appending another file that exports the public API.

LittleCMS should be present in the root directory as `./Little-CMS`. You can clone this repository with `--recurse-submodules` or clone [LittleCMS](https://github.com/mm2/Little-CMS/) directly.

Finally, run the build script with `npm run build`. The build script is a NodeJS script in `scripts/build-package.js`. You can check if the build works by running the previous demo right after.

## Acknowledgements

js-lcms is based on the following libraries:

- [lcms.js](https://github.com/yoya/lcms.js/) - Copyright (c) 2018 yoya@awm.jp. MIT License.
- [LittleCMS](https://github.com/mm2/Little-CMS/) - Copyright (c) 1998-2020 Marti Maria Saguer. MIT License.
