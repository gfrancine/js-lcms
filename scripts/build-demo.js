const esbuild = require("esbuild");
const { wasmLoader } = require("esbuild-plugin-wasm");

esbuild.build({
  entryPoints: ["demo/index.js"],
  external: ["fs", "path"],
  format: "esm",
  bundle: true,
  outfile: "demo/dist/bundle.js",
  plugins: [wasmLoader()],
});
