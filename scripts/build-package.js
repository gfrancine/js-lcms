(async () => {
  const fs = require("fs");
  const path = require("path");
  const { spawn } = require("child_process");
  const { minify } = await import("minify");

  const DIST_DIR = "dist";
  const WRAPPER_SOURCE = path.join("src", "index.js");

  const EMCC = "emcc";
  const SRC_DIR = "Little-CMS/src";
  const INCLUDE_DIR = "Little-CMS/include";
  const EXPORTED_RUNTIME_METHODS =
    "[" + ["UTF8ToString", "ccall"].map((v) => `"${v}"`).join(",") + "]";
  const EXPORTED_FUNCTIONS =
    "[" +
    [
      "free",
      "cmsOpenProfileFromMem",
      "cmsCloseProfile",
      "cmsCreate_sRGBProfile",
      "cmsCreateXYZProfile",
      "cmsCreateLab4Profile",
      "cmsGetProfileInfoASCII",
      "cmsGetColorSpace",
      "cmsFormatterForColorspaceOfProfile",
      "cmsCreateTransform",
      "cmsCreateProofingTransform",
      "cmsDeleteTransform",
      "cmsDoTransform",
      "cmsGetTransformInputFormat",
      "cmsGetTransformOutputFormat",
      "cmsReadTag",
      "cmsXYZ2xyY",
    ]
      .map((v) => `"_${v}"`)
      .join(",") +
    "]";

  const command = (cmd, args) =>
    new Promise((resolve, reject) => {
      const ls = spawn(cmd, args, {
        shell: true,
        env: process.env,
      });
      ls.stdout.on("data", (data) => {
        console.log(`stdout: ${data}`);
      });
      ls.stderr.on("data", (data) => {
        console.error(`stderr: ${data}`);
      });
      ls.on("close", (code) => {
        if (code !== 0) {
          reject(new Error("Exited with code " + code));
        } else {
          resolve();
        }
      });
    });

  // rm -rf pkg
  // mkdir pkg
  fs.rmdirSync(DIST_DIR, { recursive: true, force: true });
  fs.mkdirSync(DIST_DIR);

  // Build lcms
  console.log("building lcms");
  await command(
    EMCC,
    [
      `-o ${DIST_DIR}/lcms.js`,
      `-I ${INCLUDE_DIR} ${SRC_DIR}/*.c`,
      `-s EXPORTED_FUNCTIONS=${EXPORTED_FUNCTIONS}`,
      `-s EXPORTED_RUNTIME_METHODS=${EXPORTED_RUNTIME_METHODS}`,
      "-s ASSERTIONS=1",
      "-s TOTAL_MEMORY=33554432",
      "-s WASM=0",
      "-s ENVIRONMENT=web",
      "-s WASM_ASYNC_COMPILATION=0",
    ]
      .join(" ")
      .split(" ")
  );

  // cat pkg/lcms.js wrapper.js > pkg/index.js
  // rm pkg/lcms.js
  console.log("joining files");
  fs.writeFileSync(
    path.join(DIST_DIR, "index.js"),
    fs.readFileSync(path.join(DIST_DIR, "lcms.js")).toString("utf-8") +
      fs.readFileSync(WRAPPER_SOURCE).toString("utf-8")
  );
  fs.rmSync(path.join(DIST_DIR, "lcms.js"));

  // minify pkg/lcms.js
  console.log("minifying");
  fs.writeFileSync(
    path.join(DIST_DIR, "index.js"),
    await minify(path.join(DIST_DIR, "index.js"))
  );
})();
