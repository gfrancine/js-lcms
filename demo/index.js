import {
  cmsCreate_sRGBProfile,
  cmsCreateTransform,
  cmsDoTransform,
  cmsFormatterForColorspaceOfProfile,
  cmsGetProfileInfoASCII,
  cmsInfoDescription,
  cmsReadTag_XYZ,
  cmsSigRedColorantTag,
  cmsXYZ2xyY,
  INTENT_RELATIVE_COLORIMETRIC,
  cmsDeleteTransform,
  cmsCloseProfile,
  cmsOpenProfileFromMem,
} from "..";

const profile = cmsCreate_sRGBProfile();
const text = cmsGetProfileInfoASCII(profile, cmsInfoDescription, "en", "US");
console.log(text);

const rXYZ = cmsReadTag_XYZ(profile, cmsSigRedColorantTag);
const rxyY = cmsXYZ2xyY(rXYZ);
console.log(rXYZ, rxyY);

const inputProfile = profile;
const outputProfile = profile;
const isFloat = 0; // false
const inputFormat = cmsFormatterForColorspaceOfProfile(
  inputProfile,
  isFloat ? 0 : 2,
  isFloat
);
const outputFormat = cmsFormatterForColorspaceOfProfile(
  outputProfile,
  isFloat ? 0 : 2,
  isFloat
);

const transform = cmsCreateTransform(
  profile,
  inputFormat,
  profile,
  outputFormat,
  INTENT_RELATIVE_COLORIMETRIC,
  0
);

const rgbaImage = new Uint8Array(
  await (
    await fetch("https://images.unsplash.com/photo-1641233563119-b68ba28e9d3e")
  ).arrayBuffer()
);

console.time("convert rgba to rgb");
const rgbImage = new Uint8Array((rgbaImage.length / 4) * 3);
for (let i = 0; i < rgbaImage.length; i += 4) {
  let j = ((i + 4) / 4) * 3 - 3;
  rgbImage[j] = rgbaImage[i];
  rgbImage[j + 1] = rgbaImage[i + 1];
  rgbImage[j + 2] = rgbaImage[i + 2];
}
console.timeEnd("convert rgba to rgb");

console.time("transform rgb");
const transformedRgbImage = cmsDoTransform(
  transform,
  rgbImage,
  rgbImage.length / 3
);
console.timeEnd("transform rgb");
console.log(rgbImage, transformedRgbImage);

cmsDeleteTransform(transform);
cmsCloseProfile(profile);

const notworking = cmsOpenProfileFromMem(new Uint8Array(1), 1);
console.log(notworking); // will print 0, a null pointer
cmsCloseProfile(notworking);
